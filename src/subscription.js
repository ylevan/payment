const {Router} = require('express')
const config = require('./config')
const stripe = require('stripe')(config.PRIVATE_KEY)

const routes = Router()

routes.post('/', async (req, res) => {
  const body = req.body
  const subscription = await stripe.subscriptions.create({
    customer: 'cus_Ks2wDTP6nwQHTq',
      items: [{
        price: "price_1KCGWOLgTS9xJsg45xXTiQst",
      }],
      payment_behavior: 'default_incomplete',
      expand: ['latest_invoice.payment_intent'],
  });
  res.status(200).send(subscription)
})

module.exports = routes