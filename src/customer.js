const {Router} = require('express')
const config = require('./config')
const stripe = require('stripe')(config.PRIVATE_KEY)

const routes = Router()

routes.post('/', async (req, res) => {
  const customer = await stripe.customers.create({
    description: 'C_Van Y',
    email: 'yyy@gmail.com',
    phone: '0998888777'
  });
  res.status(200).send(customer)
})

module.exports = routes