const PRIVATE_KEY = 'sk_test_51KCF8iLgTS9xJsg41PbEfl9KWDKIOFIMHW1MFEuVZdA5GRs3OX0wUrCcQE2ztxnaJy9iVZrGzP3DfKFz4xW3GHqT00Fe10k56X'
const PUBLIC_KEY = 'pk_test_51KCF8iLgTS9xJsg4Bl1NQhA8bz9H7baTJo34OQFxOhnSk4k92THjADY0mOiN522bqjacAgLlYOAhRKLRmRQvsNOL003TCqGqFu'

const stripe = require('stripe')(PRIVATE_KEY)
const express = require('express')
const productRoutes = require('./product')
const customerRoutes = require('./customer')
const subscriptionRoutes = require('./subscription')

const app = express()

app.listen(4000)

const bootstrap = async () => {
  const storeItems = [
    { priceInCents: 10000, name: "Learn React Today", quantity: 1 },
    { priceInCents: 20000, name: "Learn CSS Today", quantity: 2 },
  ]

  const session = await stripe.checkout.sessions.create({
    payment_method_types: ["card"],
    mode: 'subscription',
    line_items: [
      {
        price: "price_1KCGWOLgTS9xJsg45xXTiQst",
        quantity: 1,
      }
    ],
    client_reference_id: "cus_Ks2wDTP6nwQHTq",
    success_url: `http://localhost:3000/success.html`,
    cancel_url: `http://localhost:3000/cancel.html`,
  })

  return session.url
}

app.post('/stripe/webhook', express.raw({type: 'application/json'}), (req, res) => {
  const sig = request.headers['stripe-signature'];

  let event;

  try {
    event = stripe.webhooks.constructEvent(request.body, sig, endpointSecret);
  } catch (err) {
    response.status(400).send(`Webhook Error: ${err.message}`);
    return;
  }

  console.log('event', event)
  res.status(200).send()
})

app.post('/stripe/payment', async (req, res) => {
  const url = await bootstrap()
  res.status(200).send(url)
})

app.use('/stripe/product', productRoutes)
app.use('/stripe/customer', customerRoutes)
app.use('/stripe/subscription', subscriptionRoutes)