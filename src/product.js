const {Router} = require('express')
const config = require('./config')
const stripe = require('stripe')(config.PRIVATE_KEY)

const routes = Router()

routes.post('/', async (req, res) => {
  const product = await stripe.products.create({
    name: 'Rent 123456 house',
    description: 'Rent house payment',
  })
  const price = await stripe.prices.create({
    product: product.id,
    unit_amount: 2000,
    currency: 'usd',
    recurring: {
      interval: 'month'
    }
  })
  res.status(200).send({...product, price})
})

module.exports = routes